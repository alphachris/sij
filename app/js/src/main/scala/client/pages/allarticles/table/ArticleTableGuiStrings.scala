package client.pages.allarticles.table

import client.pages.articleform.ArticleFormGuiStrings
import shared.pages.GuiString
import slogging.LazyLogging

/**
  * These strings appear for the user to read in the table showing all articles of all merchants and suppliers.
  * <p>
  * Created by Matthias Braun on 12/12/2016.
  */
object ArticleTableGuiStrings extends LazyLogging {

  val articleNrInCatalog = GuiString("Artikelnummer")

  val pageInCatalogOfArticle = GuiString("Seite")

  val positionInCatalog = GuiString("Anord.")

  val articleName: GuiString = ArticleFormGuiStrings.ArticleStrings.name
  val articleDescription: GuiString = ArticleFormGuiStrings.ArticleStrings.description

  val remarksForCatalogCreation: GuiString = ArticleFormGuiStrings.remarksForCatalogCreation

  val articleNrByMerchantOrSupplier: GuiString = ArticleFormGuiStrings.ArticleStrings.number

}
