package client.pages.allarticles

import client.sharedgui.SharedCss

import scalacss.Defaults._

/**
  * The CSS for the page showing all articles from all merchants and suppliers.
  * <p>
  * Created by Matthias Braun on 12/16/2016.
  */
object AllArticlesCss extends SharedCss {

  import dsl._

  "body" - defaultBackgroundColor

  forStyle(AllArticlesClasses.tableAndControls) - paddingLeft(0.5 em)

  forStyle(AllArticlesClasses.infoBox) - (
    backgroundColor.lightcyan,
    color.darkslategray,
    display.inlineBlock,
    padding(10 px, 10 px, 10 px, 10 px),
    marginTop(5 px)
  )

}
