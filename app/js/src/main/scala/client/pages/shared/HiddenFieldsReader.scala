package client.pages.shared

import client.utils.gui.Elements
import org.scalajs.dom.html.{Div, Input}
import shared.pages.HiddenFields
import slogging.LazyLogging

/**
  * Reads hidden fields from a web page. The server can write data for the client into those hidden fields.
  * <p>
  * Created by Matthias Braun on 11/3/2016.
  */
object HiddenFieldsReader extends LazyLogging {

  private def getHiddenFields: Seq[Input] = Elements.get[Div](HiddenFields.divWithFields)
    .map(hiddenFieldsDiv =>
      Elements.mapOverCollection(hiddenFieldsDiv.children, field => Elements.as[Input](field)).flatten
    ).getOrElse {
    logger.warn("Could not find hidden values in page")
    Seq()
  }

  def parseHiddenFields: Map[String, String] = getHiddenFields.map(input => (input.name, input.value)).toMap

}
