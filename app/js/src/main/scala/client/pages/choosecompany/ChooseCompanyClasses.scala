package client.pages.choosecompany

import client.sharedgui.SharedHtmlClasses

/**
  * HTML classes for the [[ChooseCompany]] page.
  * <p>
  * Created by Matthias Braun on 11/3/2016.
  */
object ChooseCompanyClasses extends SharedHtmlClasses {

  val grid = fromClassName("pure-g")

  // 5 cells go into one row
  val gridCell = fromClassName("pure-u-1-5")

  val merchantButton = fromClassName("merchant-button")

  val supplierButton = fromClassName("supplier-button")

  val merchantsAndSuppliers = fromClassName("merchants-and-suppliers")
}
