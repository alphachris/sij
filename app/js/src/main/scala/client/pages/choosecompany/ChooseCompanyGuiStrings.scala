package client.pages.choosecompany

import shared.pages.GuiString

/**
  * Strings the user will read on the [[ChooseCompany]] page.
  * <p>
  * Created by Matthias Braun on 11/3/2016.
  */
object ChooseCompanyGuiStrings {

  val chooseCompany = GuiString("Wählen Sie ein Unternehmen")

  val merchants = GuiString("Händler")
  val suppliers = GuiString("Lieferanten")

}
