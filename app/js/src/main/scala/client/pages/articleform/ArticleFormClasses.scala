package client.pages.articleform

import client.sharedgui.SharedHtmlClasses

import scalacss.Defaults._

/**
  * Contains the CSS classes we use for the [[ArticleForm]].
  * <p>
  * Created by Matthias Braun on 9/11/2016.
  *
  * @see [[ArticleFormCss]]
  */
object ArticleFormClasses extends SharedHtmlClasses {
  val valueChangedSincePreviousVersion: StyleA = fromClassName("value-has-changed-since-previous-version")

  val defaultBackgroundAndCentered: StyleA = fromClassName("default-background")

  val uploadButtonWithRemoveFilesButton: StyleA = fromClassName("upload-button-with-remove-files-button")

  val galleryAndClearButton: StyleA = fromClassName("gallery-and-clear-button")

  val pdfPreview: StyleA = fromClassName("pdf-preview")

  val left: StyleA = fromClassName("left-button")

  val thumbnail: StyleA = fromClassName("thumbnail")

  val uploadLabelWithFileInput: StyleA = fromClassName("file-upload-label-with-file-input")

  val center: StyleA = fromClassName("center")

  val table: StyleA = fromClassNames("pure-table", center.htmlClass)

  val imageGallery: StyleA = fromClassName("image-gallery")


  val alignedForm: StyleA = fromClassNames("pure-form", "pure-form-aligned")

  val smallButton: StyleA = fromClassName("button-small")

  val uploadButton: StyleA = fromClassName("upload-button")

  val submitButton = Seq(pureButton, primaryButton, largeButton)

  val cancelButton: StyleA = pureButton


  /**
    * This text informs the user to help them fill out the form.
    */
  val infoText: StyleA = fromClassName("info-text")

  val fileUploadLabel: StyleA = fromClassName("custom-file-upload")


}
