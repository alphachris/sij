package client.pages.articleform

import client.sharedgui.SharedIcons


/**
  * Provides icons for the [[ArticleForm]].
  * <p>
  * Created by Matthias Braun on 10/16/2016.
  */
object ArticleFormIcons extends SharedIcons {

  val upload = fontAwesome("fa-cloud-upload")
  val info = fontAwesome("fa-info-circle")
  val send = fontAwesome("fa-paper-plane")
}
