package client.pages.articleform

import client.sharedgui.SharedCss
import shared.articleformdata.ArticleFormIds

import scalacss.Defaults._
import scalacss.internal.AV

/**
  * Styles the [[ArticleForm]] using CSS.
  * <p>
  * Created by Matthias Braun on 10/13/2016.
  */
object ArticleFormCss extends SharedCss {

  import dsl._

  val colorForChangedData: AV = backgroundColor.palegreen

  forStyle(ArticleFormClasses.valueChangedSincePreviousVersion) - colorForChangedData

  forStyle(ArticleFormClasses.defaultBackgroundAndCentered) - (
    defaultBackgroundColor,
    width(95 %%),
    margin(auto)
  )

  forStyle(ArticleFormClasses.uploadLabelWithFileInput) - (
    // Make the upload buttons centered and left aligned
    paddingLeft(37 %%),
    textAlign.left,
    // Have some vertical space so the "remove image" button doesn't overlap with the next file upload label
    paddingTop(30 px)
  )

  "body" - (
    textAlign.center,
    defaultBackgroundColor
  )

  forStyle(ArticleFormClasses.smallButton) - fontSize(75 %%)

  forStyle(ArticleFormClasses.left) - (margin := "0 -50 -50")

  forStyle(ArticleFormClasses.imageGallery) - (

    height(250 px),
    overflow.auto,
    overflowY.hidden,
    margin(0 px, auto),
    whiteSpace.nowrap
  )

  /* Places elements like tables in the center of the screen */
  forStyle(ArticleFormClasses.center) - (
    marginLeft.auto,
    marginRight.auto
  )

  /*
  Hide the file chooser since it can't be styled. Instead, use a label for the file chooser and style that instead.
  The user can click the label which triggers the file choosing dialog.
  */
  "input[type='file']" - display.none

  forStyle(ArticleFormClasses.largeButton) - fontSize(130 %%)

  forStyle(ArticleFormClasses.infoText) - (
    color(dodgerblue),
    textShadow := getTextShadow(xOffset = 1 px, yOffset = 1 px, blurRadius = 1 px, shadowColor = rgba(0, 0, 0, 0.2))
  )

  forElement(ArticleFormIds.MerchantAndSupplierIds.data) - backgroundColor.honeydew

  forElement(ArticleFormIds.MerchantIds.merchantSpecificFiles) - backgroundColor.palevioletred

}
