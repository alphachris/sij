package client.pages.articleoverview

import shared.articleformdata.ArticleId
import shared.pages.HtmlId

/**
  * Provides [[HtmlId]]s for HTML elements of the [[ArticleOverview]] page.
  * <p>
  * Created by Matthias Braun on 11/7/2016.
  */
object ArticleOverviewIds {
  def listItem(articleId: ArticleId) = HtmlId(s"list-item-for-article-with-id-$articleId")

}
