package client.pages.articleoverview

import client.sharedgui.SharedCss

import scalacss.Defaults._

/**
  * Styles the page where the user sees their created articles.
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
object ArticleOverviewCss extends SharedCss {

  import dsl._

  "body" - defaultBackgroundColor


  // Elements inside a div with this class can be left-aligned and still be in the middle of the screen
  forStyle(ArticleOverviewClasses.center) - (
    marginLeft.auto,
    marginRight.auto,
    display.table
    )

  forStyle(ArticleOverviewClasses.articleEntry) - fontSize(130 %%)

  forStyle(ArticleOverviewClasses.userHasNoArticlesText) - (
    color.gray,
    fontSize(130 %%)
    )


  forStyle(ArticleOverviewClasses.deleteArticleLink) - (
    color.lightgray,
    fontSize(75 %%),

    &.hover - color.red
    )

  forStyle(ArticleOverviewClasses.articleList) - (
    textAlign.left,
    listStyleType := "none",
    paddingLeft(0 pt),
    paddingBottom(5 pt)
    )

}
