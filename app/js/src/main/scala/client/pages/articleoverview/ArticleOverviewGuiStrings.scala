package client.pages.articleoverview

import shared.pages.GuiString
import shared.serverresponses.{ServerResponse, ServerStringForClient}

/**
  * Strings shown to the user in the [[ArticleOverview]].
  * <p>
  * Created by Matthias Braun on 11/1/2016.
  */
object ArticleOverviewGuiStrings {

  def unexpectedServerResponse(unexpectedResponse: ServerResponse) =
    GuiString(s"Fehler beim Laden der Artikel. Server Antwort: $unexpectedResponse")

  val ajaxExceptionWhileContactingServer = GuiString(s"Could not contact server. Please check your internet connection and try again later.")

  val otherExceptionWhileContactingServer: GuiString = ajaxExceptionWhileContactingServer

  def errorGettingArticles(msg: ServerStringForClient) = GuiString(s"Fehler beim Laden der Artikel: $msg")

  val articleWithoutName = GuiString("<Artikel ohne Namen>")

  val userHasNoArticles = GuiString("Kein Artikel vorhanden; fügen Sie einen hinzu:")

  def deleteArticle(articleName: String) =
    GuiString(s"Möchten Sie '$articleName' wirklich löschen? Sie können dies nicht rückgangig machen.")

  val noMerchantOrSupplierName = GuiString("Fehler: Kein Händler- oder Lieferantenname vorhanden")

  val formTitle = GuiString("VÖW Produktkatalog 2017")

  val yourArticles = GuiString("Ihre Artikel:")

  val addArticle = GuiString("Artikel hinzufügen")
}
