package client.utils.gui.selects

/**
  * These are the values a user can select in, for example, a drop-down list.
  * <p>
  * Created by Matthias Braun on 12/12/2016.
  */
case class SelectsValue(value: String) extends AnyVal {
  override def toString: String = value
}
