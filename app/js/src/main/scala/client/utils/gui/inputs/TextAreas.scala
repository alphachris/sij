package client.utils.gui.inputs

import client.sharedgui.SharedHtmlClasses
import org.scalajs.dom.Event
import org.scalajs.dom.html.{Div, TextArea}
import shared.pages.{GuiString, HtmlId}

import scalacss.ScalatagsCss._
import scalatags.JsDom.TypedTag
import scalatags.JsDom.all._

/**
  * Creates HTML TextAreas. The user or we can be insert text in them.
  * <p>
  * Created by Matthias Braun on 12/16/2016.
  */
object TextAreas {

  private val bigTextAreaModifier = SharedHtmlClasses.toModifier(SharedHtmlClasses.bigTextAreas)
  private val bigTestAreaInRowsModifier = SharedHtmlClasses.toModifier(SharedHtmlClasses.bigTextAreaInRows)

  def veryLong(areaId: HtmlId): TypedTag[TextArea] = textarea(SharedHtmlClasses.veryLongInput)(id := areaId.value)

  def bigWithLabel(labelText: GuiString, labelId: HtmlId,
                   placeholderText: GuiString,
                   inputAttributes: Modifier*): TypedTag[Div] =
    withLabel(labelText, labelId, placeholderText, inputAttributes :+ bigTextAreaModifier)


  def emptyWithPlaceholder(textAreaId: HtmlId, placeholderText: GuiString, inputAttributes: Modifier*): TextArea =
    withPlaceholder(textAreaId, placeholderText, GuiString(""), inputAttributes)

  def withLabel(labelText: GuiString, labelId: HtmlId, placeholderText: GuiString,
                inputAttributes: Modifier*): TypedTag[Div] = div(SharedHtmlClasses.controlGroup)(
    label(`for` := labelId.value)(labelText.value),
    emptyWithPlaceholder(labelId, placeholderText, inputAttributes)
  )

  def withPlaceholder(textAreaId: HtmlId, placeholderText: GuiString, textInArea: GuiString,
                      inputAttributes: Modifier*): TextArea =
    withPlaceholder(textAreaId, placeholderText, textInArea, (_: String) => {}, inputAttributes)


  def bigWithPlaceholderInRow(textAreaId: HtmlId, placeholderText: GuiString, textInArea: GuiString,
                              onChange: (String) => Unit, inputAttributes: Modifier*): TextArea =
    withPlaceholder(textAreaId, placeholderText, textInArea, onChange, inputAttributes :+ bigTestAreaInRowsModifier)

  def withPlaceholder(textAreaId: HtmlId, placeholderText: GuiString, textInArea: GuiString,
                      onChange: (String) => Unit, inputAttributes: Modifier*): TextArea = {

    val textArea = textarea(id := textAreaId.value, placeholder := placeholderText.value,
      inputAttributes)(textInArea.value).render

    textArea.onchange = (_: Event) => onChange(textArea.value)
    textArea
  }
}
