package client.utils.gui.inputs

import org.scalajs.dom.Event
import org.scalajs.dom.html.{Button, Element}
import shared.pages.GuiString

import scalacss.ScalatagsCss._
import scalacss.internal.StyleA
import scalatags.JsDom.TypedTag
import scalatags.JsDom.all._

/**
  * Provides HTML buttons.
  * <p>
  * Created by Matthias Braun on 12/16/2016.
  */
object Buttons {

  def withTextAndIcon(text: GuiString, icon: TypedTag[Element], onClick: (Event) => Unit, styles: Seq[StyleA]): Button = {
    val textAndIcon = div(icon, span(s" $text"))
    button(styles)(`type` := "button", onclick := onClick)(textAndIcon).render
  }

  def withTextAndIcon(text: GuiString, icon: TypedTag[Element], onClick: (Event) => Unit, style: StyleA): Button =
    withTextAndIcon(text, icon, onClick, Seq(style))

  def withText(text: GuiString, onClick: (Event) => Unit, style: StyleA): Button = withTextAndIcon(text, div(), onClick, style)

}
