package data


import shared.articleformdata.ArticleFormConstants.ImageUrlConstants
import shared.articleformdata._
import shared.articleformdata.brandings.StandardArticleBrandings


object TestData {
  private val unbrandedArticleScales = examplePriceScales("unbranded article")

  private val print1c = ArticleBranding(name = StandardArticleBrandings.print1c.name, position = "mittig",
    scales = examplePriceScales(StandardArticleBrandings.print1c.name),
    initialCosts = "5 €",
    filmCosts = "1 €"
  )

  private val additionalColor = ArticleBranding(name = StandardArticleBrandings.printAdditionalColor.name,
    position = "links unten", scales = examplePriceScales(StandardArticleBrandings.printAdditionalColor.name),
    initialCosts = "2 cent", filmCosts = "0 cent")

  private val print2c = ArticleBranding(name = StandardArticleBrandings.print2c.name, position = "frontal",
    scales = examplePriceScales(StandardArticleBrandings.print2c.name),
    initialCosts = "8 €",
    filmCosts = "2 €"
  )
  private val print3c = ArticleBranding(name = StandardArticleBrandings.print3c.name, position = "links",
    scales = examplePriceScales(StandardArticleBrandings.print3c.name),
    initialCosts = "10 €",
    filmCosts = "3 €"
  )

  private val print4c = ArticleBranding(name = StandardArticleBrandings.print4c.name, position = "rechts",
    scales = examplePriceScales(StandardArticleBrandings.print4c.name),
    initialCosts = "11 €",
    filmCosts = "4 €"
  )
  private val digitalPrint4c = ArticleBranding(name = StandardArticleBrandings.digitalPrint4c.name, position = "unten",
    scales = examplePriceScales(StandardArticleBrandings.digitalPrint4c.name),
    initialCosts = "3 €",
    filmCosts = "2 €"
  )
  private val engraving = ArticleBranding(name = StandardArticleBrandings.engraving.name, position = "unten",
    scales = examplePriceScales(StandardArticleBrandings.engraving.name),
    initialCosts = "0 €",
    filmCosts = "0 €"
  )
  private val etching = ArticleBranding(name = StandardArticleBrandings.etching.name, position = "unten",
    scales = examplePriceScales(StandardArticleBrandings.etching.name),
    initialCosts = "0 €",
    filmCosts = "0 €"
  )
  private val stitching = ArticleBranding(name = StandardArticleBrandings.stitching.name, position = "unten",
    scales = examplePriceScales(StandardArticleBrandings.stitching.name),
    initialCosts = "0 €",
    filmCosts = "0 €"
  )

  private def examplePriceScales(scaleName: String) = Seq(
    ArticlePriceScale(s"$scaleName scale 1", "1 - 10", "20 cent"),
    ArticlePriceScale(s"$scaleName scale 2", "11 - 100", "15 cent"),
    ArticlePriceScale(s"$scaleName scale 3", "101 - 300", "10 cent"),
    ArticlePriceScale(s"$scaleName scale 4", "300 - 500", "8 cent"),
    ArticlePriceScale(s"$scaleName scale 5", "ab 500", "6 cent")
  )

  private val standardBrandings = Seq(
    print1c,
    additionalColor,
    print2c,
    print3c,
    print4c,
    digitalPrint4c,
    engraving,
    etching,
    stitching
  )

  private def userDefinedBranding(brandingNr: Int) = {
    val name = s"Benutzerdefinierte Veredelung $brandingNr"

    ArticleBranding(name,
      position = "mittig", scales = examplePriceScales(name),
      initialCosts = "3 € ",
      filmCosts = "2 €")
  }

  private val userDefinedBrandings = (1 to ArticleFormConstants.NrOfUserDefinedBrandings).map(userDefinedBranding)

  private val brandings = standardBrandings ++ userDefinedBrandings

  private val unbrandedArticlePrice = UnbrandedArticlePrice(
    unbrandedArticleScales,
    suggestedPrice = "1 cent"
  )

  private val minimumOrderQuantities = Seq(
    MinimumOrderQuantity("Artikel ist veredelt", "10"),
    MinimumOrderQuantity("Artikel ist unveredelt", "15"),
    MinimumOrderQuantity("Vollmond", "13")
  )

  private val mainArticleImage = ArticleFormImage(url = "http://example.org/main-article-image.jpg",
    description = ArticleImages.Descriptions.mainArticleImage)

  private val brandLogo = ArticleFormImage(url = "http://example.org/brand-logo.jpg",
    description = ArticleImages.Descriptions.brandImage)

  private def additionalLogo(logoNr: Int) =
    ArticleFormImage(s"http://example.org/additional-logo-$logoNr.jpg", ArticleImages.Descriptions.additionalLogo(logoNr))

  private def certificate(certificateNr: Int) =
    ArticleFormImage(s"http://example.org/certificate-$certificateNr.jpg", ArticleImages.Descriptions.certificate(certificateNr))

  private def additionalArticleImage(imageNr: Int) =
    ArticleFormImage(s"http://example.org/additional-image-$imageNr.jpg", ArticleImages.Descriptions.additionalArticleImage(imageNr))

  private def colorSample(sampleNr: Int) =
    ArticleFormImage(s"http://example.org/color-sample-$sampleNr.jpg", ArticleImages.Descriptions.colorSamples(sampleNr))

  private val additionalLogos = (1 to ImageUrlConstants.NrOfAdditionalLogoUrls).map(additionalLogo)
  private val certificates = (1 to ImageUrlConstants.NrOfCertificateUrls).map(certificate)
  private val additionalArticleImages = (1 to ImageUrlConstants.NrOfAdditionalArticleImageUrls).map(additionalArticleImage)

  private val colorSamples = (1 to ImageUrlConstants.NrOfColorSampleUrls).map(colorSample)

  private val images = (mainArticleImage +: (additionalArticleImages ++ colorSamples) :+ brandLogo) ++ additionalLogos ++ certificates

  private val merchantSpecificFiles = Seq(
    MerchantSpecificFile(url = "http://example.org/cover.pdf",
      description = MerchantSpecificFiles.Descriptions.cover),
    MerchantSpecificFile(url = "http://example.org/custom-pages.pdf",
      description = MerchantSpecificFiles.Descriptions.customPages)
  )

  private def exampleArticle: PromotionalArticle = PromotionalArticle(
    articleNumberAssignedByMerchantOrSupplier = "1",
    name = "Beispiel-Artikel",
    description = "Beispiel-Artikel Beschreibung",
    size = "20mm x 30mm",
    colorAndShape = "schwarzweiß"
  )

  private def catalogData: CatalogData =
    CatalogData(remarksOnCatalogCreation = "Danke!", positionInCatalog = -1, CatalogCategory.unassigned,
      CatalogSubCategory.unassigned, pageInCatalog = -1,
      articleNrInCatalog = "")

  /**
    * Creates an instance of [[ArticleFormData]] that has content in all of its fields.
    */
  def completeArticleFormData(merchantOrSupplier: MerchantOrSupplier) = ArticleFormData(

    merchantOrSupplier,
    exampleArticle,
    unbrandedArticlePrice,
    brandings,
    minimumOrderQuantities,
    images,
    merchantSpecificFiles,
    catalogData
  )
}
