import java.nio.file.Paths
import appbuildinfo.BuildInfo$

// Info on patterns: http://logback.qos.ch/manual/layouts.html#conversionWord
//def location = "%class.%method\\(%file:%line\\)"
def logMsgPattern = "%level: '%msg' %d{yyyy-MMM-dd HH:mm:ss}%n"

def consoleAppenderName = 'console logger'
def fileAppenderName = 'file logger'

appender(consoleAppenderName, ConsoleAppender) {
  encoder(PatternLayoutEncoder) {
    pattern = logMsgPattern
  }
}

appender(fileAppenderName, FileAppender) {
  file = getLogFile()
  encoder(PatternLayoutEncoder) {
    pattern = logMsgPattern;
  }
}

root(INFO, [consoleAppenderName, fileAppenderName])

def getLogFile() {
  // We use the log directory that is defined in build.sbt and put into BuildInfo by the sbt-buildinfo plugin
  logDir = BuildInfo$.MODULE$.logDir
  def defaultLogFileName = "sij.log";
  String logFile = Paths.get(logDir, defaultLogFileName).toString();
  println "Logging to '$logFile'"
  logFile;
}

