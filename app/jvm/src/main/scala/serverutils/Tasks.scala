package serverutils

import scala.concurrent.Promise
import scala.concurrent.duration._
import scalaz.\/
import scalaz.concurrent.Task

/**
  * Helps with [[Task]]s and [[scala.concurrent.Future]]s.
  * <p>
  * Created by Matthias Braun on 11/5/2016.
  */
object Tasks {

  def await[T](task: Task[T], timeoutLength: Duration): Throwable \/ T =
    task.unsafePerformSyncAttemptFor(timeoutLength)

  def toScalaFuture[T](future: scalaz.concurrent.Future[T]): scala.concurrent.Future[T] = {
    val p = Promise[T]

    future.unsafePerformAsync(response => {
      p.success(response)
    })
    p.future
  }
}
