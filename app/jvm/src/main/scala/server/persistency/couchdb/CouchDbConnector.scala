package server.persistency.couchdb

import com.ibm.couchdb.Res.DocOk
import com.ibm.couchdb._
import org.http4s.Status
import server.persistency.Articles
import shared.articleformdata._
import shared.reducedarticleview.ReducedViewOfArticle
import slogging.LazyLogging
import serverutils.Tasks

import scala.concurrent.duration._
import scalaz.concurrent.Future
import scalaz.{-\/, \/, \/-}

/**
  * Creates a [[CouchDbConnector]].
  */
object CouchDbConnector {
  def apply(info: DbConnectionInfo) = new CouchDbConnector(info)
}

/**
  * Connects to a CouchDB and lets us save and retrieve data such as promotional articles from it.
  * <p>
  * Created by Matthias Braun on 9/20/2016.
  */
class CouchDbConnector(info: DbConnectionInfo) extends LazyLogging {


  private val couch = CouchDb(info.hostName, port = info.port, https = info.shouldUseHttps, username = info.username,
    password = info.password)

  // Timeout used for database operations
  private val timeoutLength = Duration.create(10, MINUTES)
  private val dbApi: CouchDbApi = {

    val adminUrl = s"/_config/admins/${info.username}"
    Tasks.await(couch.client.put[String, String](adminUrl, Status.Ok, info.password), timeoutLength) match {
      case -\/(e) => logger.warn(s"Could not log in: $e")
      case \/-(_) => logger.info(s"Logged in as ${info.username} to CouchDB")
    }

    val dbName = info.databaseName

    // Trying to create a database with the same name as an existing one results in an error
    createDatabaseIfItDoesNotExistSync(dbName, couch)

    // Create an instance of the DB API by name and type mapping
    val api = couch.db(dbName, CouchDbQueries.typeMapping)

    createDesignDocuments(api)

    api
  }


  def delete(articleId: ArticleId): Future[\/[Throwable, Seq[DocOk]]] = {

    CouchDbQueries.filterByArticleId(articleId, dbApi).get.flatMap {
      case -\/(e) => logger.warn(s"Can't get versions of article with ID $articleId. Exception: $e")
        Future.now(-\/(e))
      //case \/-(docsToDelete) => Tasks.await(dbApi.docs.deleteMany(docsToDelete.getDocs), timeoutLength)
      case \/-(docsToDelete) => val finalResult = dbApi.docs.deleteMany(docsToDelete.getDocs).get
        finalResult

    }
  }

  //    Tasks.await(CouchDbQueries.filterByArticleId(articleId, dbApi), timeoutLength) match {
  //      case -\/(e) => logger.warn(s"Can't get versions of article with ID $articleId. Exception: $e")
  //        -\/(e)
  //      case \/-(docsToDelete) => Tasks.await(dbApi.docs.deleteMany(docsToDelete.getDocs), timeoutLength)
  //}

  def update(updatedArticleWithMetaInfo: ArticleWithMetaInfo): Future[\/[Throwable, DocOk]] = {
    val articleId = updatedArticleWithMetaInfo.metaInfo.articleId

    val docs = dbApi.docs

    val updateActions = for {
      oldDoc <- docs.get[ArticleWithMetaInfo](articleId.id)
      newDoc = oldDoc.copy(doc = updatedArticleWithMetaInfo)
      updateResult <- docs.update(newDoc)

    } yield updateResult

    updateActions.get
  }


  private def createUpdatedArticle(oldArticle: ArticleWithMetaInfo, newArticles: Seq[ReducedViewOfArticle]): ArticleWithMetaInfo = {
    val oldArticleId = oldArticle.metaInfo.articleId

    newArticles.find(_.articleId == oldArticleId).fold {
      logger.warn(s"Can't update latest version of article with ID $oldArticleId since none of the new articles has that ID")
      oldArticle
    } {
      newArticle => {
        val newCatalogData = newArticle.catalogData

        val newPromotionalArticle = oldArticle.articleFormData.articleData.copy(
          articleNumberAssignedByMerchantOrSupplier = newArticle.articleNrAssignedByPartner, name = newArticle.articleName,
          description = newArticle.articleDescription)

        val newFormData = oldArticle.articleFormData.copy(articleData = newPromotionalArticle, catalogData = newCatalogData)

        val oldVersionCount = oldArticle.metaInfo.versionCount
        val newVersionCount = oldVersionCount.increment

        val newMetaInfo = oldArticle.metaInfo.copy(versionCount = newVersionCount,
          creationDate = getNewArticleCreationDate)
        oldArticle.copy(articleFormData = newFormData, metaInfo = newMetaInfo)
      }
    }
  }

  private def getNewArticleCreationDate = System.currentTimeMillis

  private def updateArticlesAndIncreaseVersion(newArticles: Seq[ReducedViewOfArticle], oldArticles: Seq[ArticleWithMetaInfo]) =
    // Update the latest version of the docs with the content of the new articles
    oldArticles.map(oldArticle => createUpdatedArticle(oldArticle, newArticles))

  private def saveArticles(articlesToSave: Seq[ReducedViewOfArticle]) = {
    val idsOfArticlesToUpdate = articlesToSave.map(_.articleId)
    val updateActions = for {
    // Get the old articles by the IDs of the new articles
      oldDocs <- getLatestArticlesByIds(idsOfArticlesToUpdate)
      updatedArticles = updateArticlesAndIncreaseVersion(articlesToSave, oldDocs)
      creationResult <- dbApi.docs.createMany(updatedArticles)
    } yield creationResult

    updateActions
  }

  def save(articlesToSave: Seq[ReducedViewOfArticle]): \/[Throwable, Seq[DocOk]] = {

    // We can't create all the ArticleWithMetaInfo at once (which we than save into the DB), that causes an OOM error.
    // Let's group the articles and save one group after the other
    val groupSize = 40
    val tasks = articlesToSave.grouped(groupSize)
      .map(articleGroupToSave => saveArticles(articleGroupToSave))

    val results = tasks.map(task => task.attempt.unsafePerformSync)

    val emptyListRight = \/-(Nil): Throwable \/ Seq[DocOk]
    val foldedResults = results.foldLeft(emptyListRight) {
      (either1, either2) =>
        (either1, either2)
        match {
          case (\/-(successResults1), \/-(successResults2)) => \/-(successResults1 ++ successResults2)
          case (-\/(throwable), _) => -\/(throwable)
          case (_, -\/(throwable)) => -\/(throwable)
        }
    }
    foldedResults
  }

  private def getLatestArticlesByIds(ids: Seq[ArticleId]) = {
    val dbActions = for {
      docs <- CouchDbQueries.filterByArticleIds(ids, dbApi)
      articles = docs.rows.map(_.value)
      latestVersionsOfArticles = Articles.latestArticles(articles)
    } yield latestVersionsOfArticles
    dbActions
  }

  def getArticleVersionsByArticleId(id: ArticleId): Future[\/[Throwable, Seq[ArticleWithMetaInfo]]] = {

    val dbActions = for {
      docs <- CouchDbQueries.filterByArticleId(id, dbApi)
      articles = docs.rows.map(_.value)
    } yield articles

    dbActions.get
  }

  def getAllArticlesOf(merchantOrSupplierId: MerchantOrSupplierId): Future[\/[Throwable, Seq[ArticleWithMetaInfo]]] = {

    val dbActions = for {
      docs <- CouchDbQueries.filterByMerchantOrSupplier(merchantOrSupplierId, dbApi)
      articles = docs.rows.map(_.value)
    } yield articles

    //Tasks.await(dbActions, timeoutLength)
    dbActions.get
  }

  private def toReducedViewOfArticle(parts: (String, Int, String, MerchantOrSupplier, String, CatalogData, String)): ReducedViewOfArticle = {
    val (articleId, versionCount, articleName, partner, description, catalogData, articleNrAssignedByPartner) = parts

    ReducedViewOfArticle(articleId = ArticleId(articleId), versionCount = VersionCount(versionCount),
      articleName = articleName, catalogData = catalogData,
      articleDescription = description, merchantOrSupplier = partner, articleNrAssignedByPartner = articleNrAssignedByPartner)
  }

  def getReducedViewOAllArticles: Future[\/[Throwable, Seq[ReducedViewOfArticle]]] = {
    val dbActions = for {
      docs <- CouchDbQueries.getReducedViewOfAllArticles(dbApi)
      articleParts = docs.rows.map(_.value)
    } yield articleParts.map(toReducedViewOfArticle)
    dbActions.get
  }

  def getAllArticles: Future[\/[Throwable, Seq[ArticleWithMetaInfo]]] = {

    val dbActions = for {
      docs <- CouchDbQueries.getAllArticles(dbApi)

      articles = docs.getDocsData

    } yield articles

    dbActions.get
  }

  private def createDatabaseIfItDoesNotExistSync(dbName: String, couch: CouchDb): Any = {
    couch.dbs.get(dbName).get.unsafePerformSync match {
      case -\/(_) =>
        logger.info(s"Creating database $dbName")
        couch.dbs.create(dbName).get.unsafePerformSync match {
          case \/-(_) => logger.info(s"Created $dbName successfully")
          case -\/(error) => logger.warn(s"Could not create $dbName.Error: $error")
        }
      case \/-(_) =>
        logger.info(s"Database with name $dbName already existed")
    }
  }

  def createDesignDocuments(couch: CouchDbApi): Unit = {

    import ViewsAndDesigns._
    Designs.allNames.foreach(nameOfDesign => {
      // If the design doesn't exist create it. If it exists, recreate it to make sure the newest design is in the database
      Designs.nameToDesign.get(nameOfDesign).fold {
        logger.warn(s"There is no design for this name: $nameOfDesign")
      } { designToCreate =>
        couch.design.get(nameOfDesign).unsafePerformAsync {
          case -\/(_) => logger.info(s"Creating design $nameOfDesign")
            couch.design.create(designToCreate).get.unsafePerformAsync {
              case \/-(_) => logger.info(s"Created design $nameOfDesign successfully")
              case -\/(error) => logger.warn(s"Error while creating design $nameOfDesign. Error: $error")
            }

          case \/-(existingDesign) =>
            logger.info(s"Design with name ${existingDesign.name} already exists: Replacing old design with new one.")
            couch.design.delete(existingDesign).get
              .flatMap(_ => couch.design.create(designToCreate).get).unsafePerformAsync {
              case \/-(_) => logger.info(s"Recreated design $nameOfDesign")
              case -\/(error) => logger.warn(s"Error while recreating design $nameOfDesign. Error: $error")
            }
        }
      }
    })
  }

  private def addMetaInfo(formData: ArticleFormData, uuid: String) = {
    val articleId = ArticleId(uuid)
    val firstVersion = VersionCount.firstVersion
    ArticleWithMetaInfo(formData, ArticleMetaInfo(articleId, firstVersion, getNewArticleCreationDate))
  }

  def add(articleWithMetaInfo: ArticleWithMetaInfo): Future[\/[Throwable, DocOk]] = {
    val dbActions = for {
      creationResult <- dbApi.docs.create(articleWithMetaInfo)
    } yield creationResult

    dbActions.get
  }


  def add(articles: Seq[ArticleWithMetaInfo]): Future[\/[Throwable, Seq[DocOk]]] = {
    val dbActions = for {
      creationResult <- dbApi.docs.createMany(articles)
    } yield creationResult

    dbActions.get
  }

  def add(article: ArticleFormData): Future[\/[Throwable, DocOk]] = {
    val dbActions = for {
      uuid <- couch.server.mkUuid
      articleWithMetaInfo = addMetaInfo(article, uuid)
      creationResult <- dbApi.docs.create(articleWithMetaInfo, uuid)
    } yield creationResult

    dbActions.get
  }
}

/**
  * Specifies how to connect to a database.
  */
case class DbConnectionInfo(hostName: String, port: Int, databaseName: String, shouldUseHttps: Boolean,
                            username: String, password: String)

