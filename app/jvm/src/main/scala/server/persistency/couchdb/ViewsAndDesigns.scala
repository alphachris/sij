package server.persistency.couchdb

import com.ibm.couchdb.{CouchDesign, CouchView}

/**
  * Lets us filter, map, and reduce the data in our CouchDB database.
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
object ViewsAndDesigns {

  object Views {
    val byMerchantOrSupplierId = CouchView(map =
      s"""
         |function(doc) {
         | if('articleFormData' in doc.doc) {
         |   var formData = doc.doc.articleFormData;
         |   if('merchantOrSupplier' in formData && 'id' in formData.merchantOrSupplier) {
         |      var merchantOrSupplierId = formData.merchantOrSupplier.id.id;
         |      emit(merchantOrSupplierId, doc.doc);
         |   }
         | }
         |}""".stripMargin)


    val byArticleId = CouchView(map =
      s"""
         |function(doc) {
         |  if('metaInfo' in doc.doc) {
         |    var metaInfo = doc.doc.metaInfo;
         |    if('articleId' in metaInfo && 'id' in metaInfo.articleId){
         |      var articleId = metaInfo.articleId.id;
         |      emit(articleId, doc.doc);
         |    }
         |  }
         |}""".stripMargin)

    val byKind = CouchView(map =
      s"""
         |function(doc) {
         |  emit([doc.kind, doc._id], doc._id);
         |}""".stripMargin)

    val partsOfReducedArticle = CouchView(
      map =
        s"""
           |function(doc) {
           |  if(doc !== null && doc.kind == '${CouchDbQueries.articleDocKind}') {
           |    var metaInfo = doc.doc.metaInfo;
           |    var formData = doc.doc.articleFormData;
           |    var articleData = formData.articleData;
           |    var articleId = metaInfo.articleId.id;
           |    var articleName = articleData.name;
           |    var versionCount = metaInfo.versionCount.count;
           |    var catalogData = formData.catalogData;
           |    var partner = formData.merchantOrSupplier;
           |    var description = articleData.description;
           |    var articleNrAssignedByPartner = articleData.articleNumberAssignedByMerchantOrSupplier;
           |
           |    emit(articleId, [articleId, versionCount, articleName, partner, description, catalogData, articleNrAssignedByPartner]);
           |  }
           |}""".stripMargin
    )

    object Names {
      val filterByMerchantOrSupplierId = "byMerchantOrSupplierId"
      val filterByArticleId = "byArticleId"
      val filterByKind = "byKind"
      val partsOfReducedArticle = "partsOfReducedArticle"
    }
  }

  object Designs {
    def nameToDesign = Map(
      Names.filterByMerchantOrSupplierId -> byMerchantOrSupplierId,
      Names.filterByArticleId -> byArticleId,
      Names.filterByKind -> byKind,
      Names.partsOfReducedArticle -> partsOfReducedArticle
    )

    val byArticleId = CouchDesign(
      name = Names.filterByArticleId,
      views = Map(Views.Names.filterByArticleId -> Views.byArticleId)
    )

    val byMerchantOrSupplierId = CouchDesign(
      name = Names.filterByMerchantOrSupplierId,
      views = Map(Views.Names.filterByMerchantOrSupplierId -> Views.byMerchantOrSupplierId)
    )

    val byKind = CouchDesign(
      name = Names.filterByKind,
      views = Map(Views.Names.filterByKind -> Views.byKind)
    )
    val partsOfReducedArticle = CouchDesign(
      name = Names.partsOfReducedArticle,
      views = Map(Views.Names.partsOfReducedArticle -> Views.partsOfReducedArticle)
    )

    val all = Seq(byArticleId, byMerchantOrSupplierId, byKind, partsOfReducedArticle)

    object Names {
      val partsOfReducedArticle = "partsOfReducedArticle"
      val filterByMerchantOrSupplierId = "filterByMerchantOrSupplierIdDesign"
      val filterByArticleId = "filterByArticleIdDesign"
      val filterByKind = "filterByKindDesign"
    }

    val allNames = Seq(Names.filterByMerchantOrSupplierId, Names.filterByArticleId, Names.filterByKind,
      Names.partsOfReducedArticle)
  }

}
