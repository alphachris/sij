package server

import appbuildinfo.BuildInfo
import slogging._

/**
  * Starts this application.
  */
object Main extends LazyLogging {

  private def printRuntimeInfo() = {
    val runtime = Runtime.getRuntime
    val mb = 1024 * 1024
    val maxMemoryInMb = runtime.maxMemory() / mb
    logger.info(s"JVM max memory: $maxMemoryInMb MB")
  }


  private def printAppInfo() = {
    logger.info(s"App version: ${BuildInfo.version}")
    logger.info(s"App name: ${BuildInfo.name}")
  }

  def main(args: Array[String]): Unit = {

    // Without this, we wouldn't have log messages
    LoggerConfig.factory = SLF4JLoggerFactory()

    printAppInfo()
    printRuntimeInfo()

    Server.up()
  }
}
