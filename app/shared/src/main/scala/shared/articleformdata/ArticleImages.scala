package shared.articleformdata

/**
  * Information about the images for an article which the user can include in the [[ArticleForm]].
  * <p>
  * Created by Matthias Braun on 10/26/2016.
  */
object ArticleImages {

  /**
    * Descriptions of what the images show. This information is saved in the database.
    */
  object Descriptions {
    val mainArticleImage = "main image of article"

    def additionalArticleImage(imageNr: Int) = s"additional image $imageNr"

    def colorSamples(imageNr: Int) = s"color sample $imageNr"

    val brandImage = "brand image"

    def additionalLogo(imageNr: Int) = s"additional logo $imageNr"

    def certificate(imageNr: Int) = s"certificate $imageNr"
  }

}
