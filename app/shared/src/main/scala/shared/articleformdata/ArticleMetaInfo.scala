package shared.articleformdata


/**
  *
  * Meta information about a promotional article like its ID in the database, and the date it was last changed.
  * <p>
  * Created by Matthias Braun on 11/9/2016.
  *
  * @param articleId    the ID of the article
  * @param creationDate when the article was created. We use the milliseconds since the Unix epoch as way of representing
  *                     that point in time since we haven't found a time object that can be shared between server
  *                     and client
  */
case class ArticleMetaInfo(articleId: ArticleId, versionCount: VersionCount, creationDate: Long)

case class VersionCount(count: Int) extends AnyVal with Ordered[VersionCount] {
  def increment = VersionCount(count + 1)

  override def compare(that: VersionCount): Int = count.compare(that.count)
}

object VersionCount {
  // We start versions with this count
  val firstVersion = VersionCount(0)
}

