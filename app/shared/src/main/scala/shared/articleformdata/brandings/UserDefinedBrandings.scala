package shared.articleformdata.brandings

/**
  * Helps with brandings of the [[ArticleForm]] which the user have defined.
  * <p>
  * Created by Matthias Braun on 10/25/2016.
  */
object UserDefinedBrandings {
  def defaultName(userDefinedBrandingNr: Int) = s"user-defined branding $userDefinedBrandingNr"
}
