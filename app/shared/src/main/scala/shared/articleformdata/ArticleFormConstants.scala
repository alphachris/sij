package shared.articleformdata

/**
  * Some constants for the [[ArticleForm]].
  * <p>
  * Created by Matthias Braun on 10/13/2016.
  */
object ArticleFormConstants {

  object ImageUrlConstants {
    val NrOfCertificateUrls = 3

    val NrOfAdditionalLogoUrls = 3

    val NrOfColorSampleUrls = 3

    val NrOfAdditionalArticleImageUrls = 3
  }

  /**
    * A table in the article form has this many number of price scales (and thus, columns) per default
    */
  val NrOfScales = 5

  /**
    * The user can describe this many other kinds of brandings besides the standard ones (e.g., print, engraving, etching)
    */
  val NrOfUserDefinedBrandings = 5

  /**
    * The user can define this many minimum order quantities for a promotional article. Those quantities are associated
    * with conditions that must be fulfilled (for example, "branded", "not branded").
    */
  val NrOfMinimumOrderQuantities = 3

  val ArticleDescriptionMaxLength = 450

  object AcceptedFiles {
    // https://en.wikipedia.org/wiki/JPEG
    private val jpg = ".jpg,.jpeg,.jif,.jfif,.jfi"

    // https://en.wikipedia.org/wiki/TIFF
    private val tiff = ".tif,.tiff"

    // https://en.wikipedia.org/wiki/Encapsulated_PostScript
    private val eps = ".eps,.epsf,.epsi"

    // https://en.wikipedia.org/wiki/Adobe_Photoshop#File_format
    private val psd = ".psd"

    // https://en.wikipedia.org/wiki/Portable_Network_Graphics
    private val png = ".png"

    val ImageFileTypes = Seq(jpg, png, eps, psd, tiff).mkString(",")

    // https://en.wikipedia.org/wiki/Portable_Document_Format
    val PdfFileType = ".pdf"
  }

}
