package shared.pages

/**
  * Identifies an HTML element.
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
case class HtmlId(value: String) extends AnyVal {
  override def toString: String = value
}
