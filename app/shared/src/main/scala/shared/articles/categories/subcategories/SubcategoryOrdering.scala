package shared.articles.categories.subcategories

import shared.articles.categories.subcategories.ArticleSubcategories._

/**
  * The ordering of an article subcategory within a category is defined here.
  * <p>
  * Created by Matthias Braun on 3/28/2017.
  */
object SubcategoryOrdering {
  def sortNr(subcategory: ArticleSubcategory): Option[Int] = subcategoriesAndSortNumbers.get(subcategory)

  private val subcategoriesAndSortNumbers = {

    val office = {
      import Office._
      Map(
        ballpens -> 1,
        writingSets -> 2,
        coloredPencils -> 3,
        markerPens -> 4,
        stickyNotes -> 5,
        memoBoxes -> 6,
        notebooks -> 7,
        folders -> 8,
        mousepads -> 9,
        calculators -> 10
      )
    }
    val usbAndPowerbanksAndAccessories = {
      import UsbAndPowerbanksAndAccessories._
      Map(
        usbSticks -> 1,
        powerbanks -> 2,
        adaptersAndConnectionCables -> 3,
        solarChargers -> 4
      )
    }
    val technology = {
      import Technology._
      Map(
        loudspeakers -> 1,
        radios -> 2,
        drones -> 3
      )
    }
    val bags = {
      import Bags._
      Map(
        carryingBags -> 1,
        travelBags -> 2,
        shoulderBags -> 3,
        rucksacks -> 4
      )
    }

    val food = {
      import Food._
      Map(
        sweets -> 1,
        saltyFood -> 2,
        drinks -> 3,
        tea -> 4,
        giftSets -> 5
      )
    }

    val utilities = {
      import Utilities._
      Map(
        matchsticks -> 1,
        lighters -> 2,
        keychains -> 3,
        coinBanks -> 4,
        beauty -> 5,
        wallets -> 6,
        sundries -> 7,
        phoneAccessories -> 8,
        multitool -> 9,
        torches -> 10,
        umbrellas -> 11
      )
    }

    val leisureAndGames = {
      import LeisureAndGames._
      Map(
        fitnessAccessories -> 1,
        drinkingBottles -> 2,
        thermosbottles -> 3,
        leisure -> 4,
        plushToys -> 5,
        fanMerchandise -> 6
      )
    }

    val homeAndTravel = {
      import HomeAndTravel._
      Map(
        porcelainAndCeramics -> 1,
        glasswareAndCarafes -> 2,
        stockCans -> 3,
        kitchenAccessories -> 4,
        teaAccessories -> 5,
        giftSets -> 6,
        growingThings -> 7,
        blankets -> 8,
        watches -> 9,
        shoppingBasketsAndBoxes -> 10,
        suitCases -> 11,
        toiletryBags -> 12
      )
    }

    val textiles = {
      import Textiles._
      Map(
        tShirts -> 1,
        poloShirts -> 2,
        shirtsAndBlouses -> 3,
        pullovers -> 4,
        fleece -> 5,
        softshell -> 6,
        jackets -> 7,
        hatsAndCaps -> 8,
        sportClothes -> 9,
        traditionalCostumes -> 10,
        workClothes -> 11,
        tiesAndScarfs -> 12,
        terrycloths -> 13
      )
    }

    office ++ usbAndPowerbanksAndAccessories ++ technology ++ bags ++ food ++ utilities ++ leisureAndGames ++
      homeAndTravel ++ textiles + (ArticleSubcategories.none -> Integer.MAX_VALUE)
  }
}
