package shared.reducedarticleview

import shared.articleformdata.{ArticleId, CatalogData, MerchantOrSupplier, VersionCount}

/**
  * An article that doesn't contain all the data of an [[shared.articleformdata.ArticleWithMetaInfo]].
  * The reduced view is used for performance reasons on the page that lets the user sort and categorize all articles.
  * <p>
  * Created by Matthias Braun on 4/11/2017.
  */
case class ReducedViewOfArticle(articleId: ArticleId, articleName: String, catalogData: CatalogData,
                                articleDescription: String, versionCount:VersionCount,
                                merchantOrSupplier: MerchantOrSupplier,
                                articleNrAssignedByPartner: String)

