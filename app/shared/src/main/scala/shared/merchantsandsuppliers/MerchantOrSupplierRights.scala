package shared.merchantsandsuppliers

import shared.Deadlines
import shared.articleformdata.{Merchant, MerchantOrSupplier, Supplier}

/**
  * Defines what a [[shared.articleformdata.MerchantOrSupplier]] is allowed to do.
  *
  * @param id the ID of the right
  */
case class MerchantOrSupplierRight(id: MerchantOrSupplierRightId)

object MerchantOrSupplierRights {

  def rightsFor(merchantOrSupplier: MerchantOrSupplier): Seq[MerchantOrSupplierRight] =
    merchantOrSupplier match {
      case _: Merchant =>
        // After the deadline has passed, merchants can't change, add, or delete articles
        if (Deadlines.merchantDeadlineHasPassed) Seq()
        else
          MerchantOrSupplierRights.all

      case _: Supplier =>
        // After the deadline has passed, suppliers can't change, add, or delete articles
        if (Deadlines.supplierDeadlineHasPassed) Seq()
        else MerchantOrSupplierRights.all
    }

  def toRights(rightIds: Seq[MerchantOrSupplierRightId]): Seq[MerchantOrSupplierRight] = rightIds.flatMap(rightsById.get)

  // A short alias for creating MerchantOrSupplierRightIds
  private def id = MerchantOrSupplierRightId

  val addArticle = MerchantOrSupplierRight(id("add-article-right"))
  val deleteArticle = MerchantOrSupplierRight(id("delete-article-right"))
  // The right for submitting a new or a changed article
  val submitArticle = MerchantOrSupplierRight(id("submit-article-right"))

  val all = Seq(
    addArticle,
    deleteArticle,
    submitArticle
  )

  private val rightsById = all.map(right => right.id -> right).toMap
}

case class MerchantOrSupplierRightId(value: String) extends AnyVal {
  override def toString: String = value
}



