package shared.merchantsandsuppliers

import shared.articleformdata.{Merchant, MerchantOrSupplier, MerchantOrSupplierId, Supplier}

/**
  * Information about the merchants and suppliers that can use this application.
  * <p>
  * Created by Matthias Braun on 11/3/2016.
  */
object MerchantsAndSuppliers {
  def isSupplier(merchantOrSupplier: MerchantOrSupplier): Boolean = suppliers.contains(merchantOrSupplier)

  def isSupplier(id: MerchantOrSupplierId): Boolean = suppliers.map(supplier => supplier.id).contains(id)

  def isMerchant(merchantOrSupplier: MerchantOrSupplier): Boolean = merchants.contains(merchantOrSupplier)

  def isMerchant(id: MerchantOrSupplierId): Boolean = merchants.map(merchant => merchant.id).contains(id)


  // A short alias for creating MerchantOrSupplierIds
  private def id = MerchantOrSupplierId

  // The fallback merchant if we don't know whose merchant's data we should show
  val unknownMerchant = Merchant("Unbekannt", id("unknown"))

  // These merchants are shown at the "Choose Company" page. The ID is used in the URL leading to the merchant's article overview
  val merchants = Seq(
    Merchant("Seybold", id("yboxdk")),
    Merchant("The Five Elements", id("elemsin")),
    Merchant("Kolibri", id("olibnn")),
    Merchant("Istac Promotion", id("plstaczc")),
    Merchant("Wunderbaldinger", id("0xawunderxa4")),
    Merchant("HSI Promotion", id("rb9a9hsixaa")),
    Merchant("Radlgruber Werbegeschenke", id("vei3radlaj")),
    Merchant("Werbegaben Hitsch", id("slhitsc94s")),
    Merchant("Kneiko", id("gafhkneik7")),
    Merchant("K12 Kalender", id("aek12kal55")),
    Merchant("AV Verlag", id("uwavverla433")),
    Merchant("Optimal Präsent", id("8aoptimpraes944")),
    Merchant("Pauger Werbeartikel", id("paug431b"))
  )

  // These suppliers are shown at the "Choose Company" page. The ID is used in the URL leading to the supplier's article overview
  val suppliers = Seq(
    Supplier("MAGNA sweets", id("magurr")),
    Supplier("Jung Bonbonfabrik", id("jun1o")),
    Supplier("Kalfany Süße Werbung", id("0kalfnyor")),
    Supplier("Staedtler Mars", id("82staedpkq")),
    Supplier("MBW", id("e9mbwuqi")),
    Supplier("Elasto Form", id("alwelast2a")),
    Supplier("PF Concept", id("fdrqpfconcu1")),
    Supplier("Poul Willumsen", id("giwillums59a")),
    Supplier("Bayerische Glaswerke", id("va4bayerifn")),
    Supplier("Acar Europe", id("mmacar76j")),
    Supplier("Take2 Design", id("jtatake2ir")),
    Supplier("Octogone", id("hokhoctog4pei")),
    Supplier("Spranz", id("vnr4spranqdu")),
    Supplier("Adore", id("9fador4jy")),
    Supplier("Michael Schiffer Promotion", id("yseschiffequo")),
    Supplier("Lynka Sp. z o.o.", id("gulynkei4")),
    Supplier("Senator", id("zhsenato9")),
    Supplier("Victorinox", id("jevictori43")),
    Supplier("Giving Europe", id("tkgiving11a")),
    Supplier("Spielkartenfabrik", id("p2qspielka5")),
    Supplier("Cotton Classics", id("ncottoncl9a4")),
    Supplier("Falk & Ross", id("hffalkrog0")),
    Supplier("New Wave", id("rrxnewwavjt")),
    Supplier("Framsohn", id("gaframso2i")),
    Supplier("Ritter Pen", id("u0ritte9422")),
    Supplier("Inspirion", id("jainspirge")),
    Supplier("Eurostyle", id("ax2eurostp37")),
    Supplier("WIL Langenberg", id("jlangenb61c")),
    Supplier("HEPLA", id("hepl413")),
    Supplier("Herzog Products", id("12herzo8by")),
    Supplier("Mahlwerck", id("s9kmahlwera2")),
    Supplier("Reflects", id("8afrefle8di")),
    Supplier("Nestler Matho", id("axmatho4e")),
    Supplier("Europe Match", id("iw1europematc93"))
  )

  private val merchantsAndSuppliersByName: Map[String, MerchantOrSupplier] = {
    val merchantMap = merchants.map(merch => merch.name -> merch).toMap
    val supplierMap = suppliers.map(supplier => supplier.name -> supplier).toMap
    merchantMap ++ supplierMap
  }

  private val merchantsAndSuppliersById: Map[MerchantOrSupplierId, MerchantOrSupplier] = {
    val merchantMap = merchants.map(merch => merch.id -> merch).toMap
    val supplierMap = suppliers.map(supplier => supplier.id -> supplier).toMap
    merchantMap ++ supplierMap
  }

  val idsOfAll: Seq[MerchantOrSupplierId] = merchantsAndSuppliersById.keys.toSeq

  def idToMerchantOrSupplier(id: MerchantOrSupplierId): Option[MerchantOrSupplier] = merchantsAndSuppliersById.get(id)

  def nameToMerchantOrSupplier(name: String): Option[MerchantOrSupplier] = merchantsAndSuppliersByName.get(name)
}

