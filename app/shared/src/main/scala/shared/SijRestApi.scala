package shared

/**
  * Used to communicate with the server using, for example, GET, PUT, and POST HTML commands.
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
object SijRestApi {

  object Parameters {
    val ArticleId = 'id
    val MerchantOrSupplierId = Symbol("merchant-or-supplier-id")
  }

  object Paths {

    val GetArticlesOfAllSuppliers = "get-articles-of-all-suppliers"

    val GetArticles = "get-articles"

    val GetArticlesAsCsv = "all-articles.csv"

    val DeleteArticle = "delete"

    val EditArticle = "edit"
  }

  // Used for performing Ajax calls from the client to the server
  val AjaxPath = "ajax"
}
