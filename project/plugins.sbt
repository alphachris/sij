// Plugins for SBT. Don't forget to leave blank lines between the plugin declarations

// Compiles Scala to JavaScript: http://www.scala-js.org
addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.12")

// Creates a jar from the source code: https://github.com/sbt/sbt-native-packager
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.1.1")

// Creates a Docker image from the jar: https://github.com/marcuslonnberg/sbt-docker
addSbtPlugin("se.marcuslonnberg" % "sbt-docker" % "1.4.1")

// Visualizes the dependency tree of this project: https://github.com/jrudolph/sbt-dependency-graph
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.8.2")

// Make information about our build, such as the application name, available in the application.
// https://github.com/sbt/sbt-buildinfo
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.7.0")
